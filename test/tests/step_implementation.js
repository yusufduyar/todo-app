/* globals gauge*/
"use strict";
const {
  openBrowser,
  write,
  closeBrowser,
  goto,
  press,
  text,
  click,
  checkBox,
  near,
  listItem,
  hover,
  button,
  toRightOf,
  link,
  toLeftOf
} = require("taiko");
const assert = require("assert");

beforeSpec(async () => {
  await openBrowser({
    headless: true,
    args: ["--no-sandbox", "--disable-setuid-sandbox"]
  });
  await goto("http://104.154.117.66");
});

afterSpec(async () => {
  await closeBrowser();
});

step("Create new task as <todoitem>", async todoitem => {
  await write(todoitem);
  await press("Enter");
});

step("Page contains <content>", async content => {
  await text(content).exists();
});

step("Mark as done <todoitem2>", async todoitem2 => {
  await click(checkBox(near(todoitem2)));
});

step("Checkbox of <todoitem> is checked", async todoitem => {
  await checkBox(toLeftOf(todoitem)).isChecked();
});

step("Is <todoitem> marked as completed", async todoitem => {
  await listItem(todoitem, { class: "completed" }).exists();
});

step("Hover to <todoitem>", async todoitem => {
  await hover(todoitem);
});

step("Click delete button near <todoitem>", async todoitem => {
  await click(button({ class: "destroy" }, toRightOf(todoitem)));
});

step("Check <todoitem> is not exists", async todoitem => {
  await !text(todoitem).exists();
});

step("Click <linktext>", async linktext => {
  await click(link(linktext));
});

step("Check <todoitem> is exists", async todoitem => {
  await text(todoitem).exists();
});

step("Click mark all as completed button", async () => {
  await click(checkBox({ class: "toggle-all" }));
});
