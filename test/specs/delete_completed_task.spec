# Delete Completed Task

## Delete Completed Task

* Create new task as "TaskToBeDeleted"
* Hover to "TaskToBeDeleted"
* Click delete button near "TaskToBeDeleted"
* Check "TaskToBeDeleted" is not exists
