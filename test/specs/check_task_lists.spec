# Check task lists

## Add New Tasks

* Create new task as "PendingTask"
* Create new task as "TaskToBeCompleted"
* Mark as done "TaskToBeCompleted"

## Check Completed List

* Click "Completed"
* Check "PendingTask" is not exists
* Check "TaskToBeCompleted" is exists

## Check Active List

* Click "Active"
* Check "PendingTask" is exists
* Check "TaskToBeCompleted" is not exists

## Check All List

* Click "All"
* Check "PendingTask" is exists
* Check "TaskToBeCompleted" is exists
