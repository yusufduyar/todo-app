# Mark Complete One Item

## Mark Complete

* Create new task as "TaskToBeCompleted"
* Mark as done "TaskToBeCompleted"
* Is "TaskToBeCompleted" marked as completed
* Checkbox of "TaskToBeCompleted" is checked
